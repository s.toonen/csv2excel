## CSV2EXCEL

A simple tool to convert one or more CSV-files to Excel XLSX. A YAML configuration
file may be provided.

### Usage

``csv2excel --config config.yaml --output result.xlsx``

### Configuration reference

```yaml
example_config: here
```
