package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/xuri/excelize/v2"
)

type Worksheet struct {
	Title        string
	Index        int
	Workbook     *excelize.File
	Config       *WorksheetConfig
	StreamWriter *excelize.StreamWriter
	ColumnCount  int
	RowCount     int
}

func createSheet(xlsx *excelize.File, title string, index int, shCfg *WorksheetConfig) *Worksheet {
	sheet := &Worksheet{
		Title:    title,
		Index:    index,
		Workbook: xlsx,
		Config:   shCfg,
	}

	if index == 0 && title != DEFAULTSHEETNAME {
		xlsx.SetSheetName(DEFAULTSHEETNAME, title)
	} else {
		xlsx.NewSheet(title)
	}

	return sheet
}

func (sh *Worksheet) setOptions() error {
	sh.Workbook.SetSheetVisible(sh.Title, true)

	prp1 := sh.Config.FormatProperties
	if err := sh.Workbook.SetSheetFormatPr(sh.Title,
		excelize.BaseColWidth(prp1.BaseColWidth),
		excelize.DefaultColWidth(prp1.DefaultColWidth),
		excelize.DefaultRowHeight(prp1.DefaultRowHeight),
		excelize.CustomHeight(prp1.CustomHeight),
		excelize.ZeroHeight(prp1.ZeroHeight),
		excelize.ThickTop(prp1.ThickTop),
		excelize.ThickBottom(prp1.ThickBottom),
	); err != nil {
		return err
	}

	prp2 := sh.Config.ViewProperties
	if err := sh.Workbook.SetSheetViewOptions(sh.Title, 0,
		excelize.DefaultGridColor(prp2.DefaultGridColor),
		excelize.ShowFormulas(prp2.ShowFormulas),
		excelize.ShowGridLines(prp2.ShowGridLines),
		excelize.ShowRowColHeaders(prp2.ShowRowColHeaders),
		excelize.RightToLeft(prp2.RightToLeft),
		excelize.TopLeftCell(prp2.TopLeftCell),
		excelize.ZoomScale(prp2.ZoomScale),
	); err != nil {
		return err
	}

	return nil
}

func (sh *Worksheet) runConversion() error {
	// Open input file
	csvFile, err := os.Open(sh.Config.Input)

	if err != nil {
		fmt.Printf("Unable to open file '%s': %s\n", sh.Config.Input, err)
		return err
	}

	defer csvFile.Close()

	if err = sh.setOptions(); err != nil {
		fmt.Printf("Unable to set options: %s\n", err)
		return err
	}

	csvReader := createCsvReader(csvFile)

	if err = sh.createHeader(csvReader); err != nil {
		fmt.Printf("Unable to copy header contents: %s\n", err)
		return err
	}

	if !sh.mustCreateTable() {
		if err = sh.createHeaderStyle(); err != nil {
			fmt.Printf("Unable to create header style: %s\n", err)
			return err
		}

		if err = sh.setAutoFilter(); err != nil {
			fmt.Printf("Unable to set auto filter: %s\n", err)
			return err
		}
	}

	if err = sh.copyContents(csvReader); err != nil {
		fmt.Printf("Unable to copy contents: %s\n", err)
		return err
	}

	if sh.mustCreateTable() {
		sh.createTable()
	} else {
		if err = sh.freezePanes(); err != nil {
			fmt.Printf("Unable to freeze panes: %s\n", err)
			return err
		}
	}

	return nil
}

func (sh *Worksheet) mustCreateTable() bool {
	return sh.Config.TableStyle != ""
}

func (sh *Worksheet) createTable() error {
	cell, _ := excelize.CoordinatesToCellName(
		sh.ColumnCount,
		sh.RowCount,
	)

	config := fmt.Sprintf(`{
		"table_name": "table",
		"table_style": "%s",
		"show_first_column": true,
		"show_last_column": false,
		"show_row_stripes": false,
		"show_column_stripes": true
	}`, sh.Config.TableStyle)

	if err := sh.Workbook.AddTable(sh.Title, "A1", cell, config); err != nil {
		fmt.Printf("Unable to create table: %s\n", err)
		return err
	}

	return nil
}

func (sh *Worksheet) freezePanes() error {
	if sh.Config.Freeze.Rows+sh.Config.Freeze.Columns == 0 {
		return nil
	}

	// Temp workaround for bug in execlize
	if sh.Config.Freeze.Columns == 1 {
		sh.Config.Freeze.Columns++
	}

	cell, err := excelize.CoordinatesToCellName(
		sh.Config.Freeze.Columns+1,
		sh.Config.Freeze.Rows+1,
	)

	if err != nil {
		return err
	}

	config := fmt.Sprintf(`{
		"freeze": true,
		"split": false,
		"x_split": %d,
		"y_split": %d,
		"top_left_cell": "%s"
	}`, sh.Config.Freeze.Rows, sh.Config.Freeze.Columns, cell)

	sh.Workbook.SetPanes(sh.Title, config)

	return nil
}

func (sh *Worksheet) createHeader(csvReader *csv.Reader) error {
	for row := 1; row <= sh.Config.Header.Rows; row++ {
		record, err := csvReader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}

		sh.RowCount++
		column := 1

		if row == 1 {
			// Detemine column count based on first row
			sh.ColumnCount = len(record)
		}

		for _, value := range record {
			cell, err := excelize.CoordinatesToCellName(column, row)

			if err != nil {
				return err
			}

			sh.Workbook.SetCellValue(sh.Title, cell, value)
			column++
		}
	}

	return nil
}

func (sh *Worksheet) setAutoFilter() error {
	if !sh.Config.Header.Autofilter {
		return nil
	}

	cell, err := excelize.CoordinatesToCellName(sh.ColumnCount, 1)

	if err != nil {
		return err
	}

	if err = sh.Workbook.AutoFilter(sh.Title, "A1", cell, ""); err != nil {
		return err
	}

	return nil
}

func (sh *Worksheet) createHeaderStyle() error {
	styleID, err := sh.Workbook.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Bold:  true,
			Color: "#333333",
		},
		Alignment: &excelize.Alignment{
			Vertical: "center",
		},
	})

	if err != nil {
		return err
	}

	cell, err := excelize.CoordinatesToCellName(sh.ColumnCount, sh.Config.Header.Rows)

	if err != nil {
		return err
	}

	sh.Workbook.SetCellStyle(sh.Title, "A1", cell, styleID)

	return nil
}

func (sh *Worksheet) copyContents(csvReader *csv.Reader) error {
	row := sh.Config.Header.Rows + 1

	// Loop trough CSV rows
	for {
		record, err := csvReader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}

		sh.RowCount++

		if err = sh.processRow(record, row); err != nil {
			fmt.Printf("Error during conversion of row %d: %v\n", row, record)
			return err
		}

		row++
	}

	return nil
}

func (sh *Worksheet) processRow(record []string, rowID int) error {
	for colID, value := range record {
		cell, err := excelize.CoordinatesToCellName(colID+1, rowID)

		if err != nil {
			return err
		}

		sh.createCell(cell, value)
	}

	return nil
}

func (sh *Worksheet) createCell(cell string, value string) {
	cellType := "text"

	if config.Autoformula && strings.HasPrefix(value, "=") {
		cellType = "formula"
	}

	switch cellType {
	case "formula":
		sh.Workbook.SetCellFormula(sh.Title, cell, value)
	default:
		sh.Workbook.SetCellValue(sh.Title, cell, value)
	}
}
