PROJECT_NAME := "csv2excel"
GO_FILES := $(shell find . -name '*.go' | grep -v vendor/ | grep -v _test.go)
TEST_FILES := $(shell find . -name '*.go' | grep -v vendor/)
#LDFLAGS=-ldflags "-X main.versionInfo=0.6.6"
LDFLAGS=

.PHONY: all test coverage
all: get build

get:
	go get ${TEST_FILES}

build: fetch ## Build the binary file
	mkdir -p build
	go build ${LDFLAGS} -o ./build/$(PROJECT_NAME) ${GO_FILES}
	chmod +x ./build/$(PROJECT_NAME)

platforms: ## Cross compile for all platforms
	GOOS=windows GOARCH=386 go build ${LDFLAGS} -o ./build/$(PROJECT_NAME)_386.exe ${GO_FILES}
	GOOS=windows GOARCH=amd64 go build ${LDFLAGS} -o ./build/$(PROJECT_NAME)_amd64.exe ${GO_FILES}
	GOOS=linux GOARCH=386 go build ${LDFLAGS} -o ./build/$(PROJECT_NAME)_linux_386 ${GO_FILES}
	GOOS=linux GOARCH=amd64 go build ${LDFLAGS} -o ./build/$(PROJECT_NAME)_linux_amd64 ${GO_FILES}
	GOOS=darwin GOARCH=amd64 go build ${LDFLAGS} -o ./build/$(PROJECT_NAME)_osx ${GO_FILES}

install: ## Copy binaries
	go install

clean: ## Remove previous build
	@rm -rf ./build/
	@rm -f .coverage.*

fetch:
	go mod download

lint: ## Lint the files
	@staticcheck ${TEST_FILES}

test: fetch ## Run unittests
	go test ${TEST_FILES} -coverprofile .coverage.txt
	go tool cover -func .coverage.txt

coverage: test
	go tool cover -html=.coverage.txt
