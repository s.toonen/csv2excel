package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/creasty/defaults"
	"gopkg.in/dealancer/validate.v2"
	"gopkg.in/yaml.v3"
)

type Options struct {
	outputFile string
	configPath string
}

type Config struct {
	Encoding    string `yaml:"encoding" default:"utf-8" validate:"one_of=utf-8,utf-16"`
	DateFormat  string `yaml:"dateformat" default:"YYYY-MM-DD"`
	Autoformula bool   `yaml:"autoformula" default:"true"`
	Reader      struct {
		Delimiter       string `yaml:"delimiter" default:";"`
		LazyQuotes      bool   `yaml:"lazyquotes" default:"false"`
		ValidateColumns bool   `yaml:"validatecolumns" default:"true"`
	} `yaml:"reader"`
	AppProps struct {
		Application       string `yaml:"application"`
		ScaleCrop         bool   `yaml:"scalecrop"`
		DocSecurity       int    `yaml:"docsecurity"`
		Company           string `yaml:"company"`
		LinksUpToDate     bool   `yaml:"linksuptodate"`
		HyperlinksChanged bool   `yaml:"hyperlinkschanged"`
		AppVersion        string `yaml:"appversion"`
	} `yaml:"application"`
	DocProps struct {
		Category       string `yaml:"category"`
		ContentStatus  string `yaml:"contentstatus"`
		Created        string `yaml:"created"`
		Creator        string `yaml:"creator"`
		Description    string `yaml:"description"`
		Identifier     string `yaml:"identifier"`
		Keywords       string `yaml:"keywords"`
		LastModifiedBy string `yaml:"lastmodifiedby"`
		Modified       string `yaml:"modified"`
		Revision       string `yaml:"revision"`
		Subject        string `yaml:"subject"`
		Title          string `yaml:"title"`
		Language       string `yaml:"language"`
		Version        string `yaml:"version"`
	} `yaml:"document"`
	Worksheets map[string]WorksheetConfig `yaml:"worksheets"`
}

type WorksheetConfig struct {
	Title      string `yaml:"title"`
	Input      string `yaml:"input"`
	TableStyle string `yaml:"table" default:""`
	Freeze     struct {
		Rows    int `yaml:"rows" default:"0"`
		Columns int `yaml:"columns" default:"0"`
	} `yaml:"freeze"`
	Header struct {
		Rows       int  `yaml:"rows" default:"1"`
		Autofilter bool `yaml:"autofilter" default:"true"`
		Underline  bool `yaml:"underline" default:"false"`
	} `yaml:"header"`
	FormatProperties struct {
		BaseColWidth     uint8   `yaml:"basecolwidth" default:"0"`
		DefaultColWidth  float64 `yaml:"defaultcolwidth" default:"0"`
		DefaultRowHeight float64 `yaml:"defaultrowheight" default:"15"`
		CustomHeight     bool    `yaml:"customheight" default:"false"`
		ZeroHeight       bool    `yaml:"zeroheight" default:"false"`
		ThickTop         bool    `yaml:"thicktop" default:"false"`
		ThickBottom      bool    `yaml:"thickbottom" default:"false"`
	} `yaml:"format"`
	ViewProperties struct {
		DefaultGridColor  bool    `yaml:"defaultgridcolor" default:"true"`
		ShowFormulas      bool    `yaml:"showformulas" default:"false"`
		ShowGridLines     bool    `yaml:"showgridlines" default:"true"`
		ShowRowColHeaders bool    `yaml:"showrowcolheaders" default:"true"`
		ShowZeros         bool    `yaml:"showzeros" default:"true"`
		RightToLeft       bool    `yaml:"righttoleft" default:"false"`
		ShowRuler         bool    `yaml:"showruler" default:"false"`
		View              string  `yaml:"view" default:"normal"`
		TopLeftCell       string  `yaml:"topleftcell" default:""`
		ZoomScale         float64 `yaml:"zoomscale" default:"0"`
	} `yaml:"view"`
}

func parseCommandLine() {
	cmdlineFlags := flag.NewFlagSet(os.Args[0], flag.ContinueOnError)
	cmdlineFlags.StringVar(&opts.outputFile, "output", "", "full pathname of the output file (XLSX file)")
	cmdlineFlags.StringVar(&opts.configPath, "config", "./config.yml", "full pathname of the configuration file (YAML file)")

	err := cmdlineFlags.Parse(os.Args[1:])

	if err != nil || !cmdlineFlags.Parsed() {
		fmt.Println("")
		fmt.Println("Could not parse the command line, invalid option or value.")
		fmt.Println("Error:", err)
		fmt.Println("")
		os.Exit(-1)
	}

	// Validate the path first
	if err := validateConfigPath(opts.configPath); err != nil {
		fmt.Println("Error:", err)
		os.Exit(-1)
	}
}

func parseConfig() {
	// Open config file
	file, err := ioutil.ReadFile(opts.configPath)
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(-1)
	}

	config = &Config{}
	err = yaml.Unmarshal(file, &config)

	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(-1)
	}

	if err := validate.Validate(&config); err != nil {
		fmt.Println("Error:", err)
		os.Exit(-1)
	}

	if len(config.Worksheets) == 0 {
		fmt.Println("At least one worksheet should be defined")
		os.Exit(-1)
	}
}

func validateConfigPath(path string) error {
	s, err := os.Stat(path)
	if err != nil {
		return err
	}
	if s.IsDir() {
		return fmt.Errorf("'%s' is a directory, not a normal file", path)
	}
	return nil
}

func (c *Config) UnmarshalYAML(unmarshal func(interface{}) error) error {
	defaults.Set(c)

	type plain Config
	if err := unmarshal((*plain)(c)); err != nil {
		return err
	}

	return nil
}

func (c *WorksheetConfig) UnmarshalYAML(unmarshal func(interface{}) error) error {
	defaults.Set(c)

	type plain WorksheetConfig
	if err := unmarshal((*plain)(c)); err != nil {
		return err
	}

	return nil
}
