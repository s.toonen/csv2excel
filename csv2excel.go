package main

import (
	"fmt"
)

var opts Options
var config *Config

const DEFAULTSHEETNAME = "Sheet1"

func main() {
	parseCommandLine()
	parseConfig()

	workbook := createWorkbook()
	sheetNr := 0

	for title, sheetConfig := range config.Worksheets {
		sheet := createSheet(workbook, title, sheetNr, &sheetConfig)

		if err := sheet.runConversion(); err != nil {
			fmt.Printf("Error during conversion of %s: %s", title, err)
		}

		sheetNr++
	}

	saveWorkbook(workbook)
}
