package main

import (
	"fmt"
	"os"

	"github.com/xuri/excelize/v2"
)

func createWorkbook() *excelize.File {
	f := excelize.NewFile()

	app := config.AppProps
	f.SetAppProps(&excelize.AppProperties{
		Application:       app.Application,
		ScaleCrop:         app.ScaleCrop,
		DocSecurity:       app.DocSecurity,
		Company:           app.Company,
		LinksUpToDate:     app.LinksUpToDate,
		HyperlinksChanged: app.HyperlinksChanged,
		AppVersion:        app.AppVersion,
	})

	props := config.DocProps
	err := f.SetDocProps(&excelize.DocProperties{
		Category:       props.Category,
		ContentStatus:  props.ContentStatus,
		Created:        props.Created,
		Creator:        props.Creator,
		Description:    props.Description,
		Identifier:     props.Identifier,
		Keywords:       props.Keywords,
		LastModifiedBy: props.LastModifiedBy,
		Modified:       props.Modified,
		Revision:       props.Revision,
		Subject:        props.Subject,
		Title:          props.Title,
		Language:       props.Language,
		Version:        props.Version,
	})

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	return f
}

func saveWorkbook(xlsx *excelize.File) {
	err := xlsx.SaveAs(opts.outputFile)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
