package main

import (
	"encoding/csv"
	"fmt"
	"os"
)

func createCsvReader(csvFile *os.File) *csv.Reader {
	csvReader := csv.NewReader(csvFile)
	csvReader.Comma = toRune(config.Reader.Delimiter)
	csvReader.LazyQuotes = config.Reader.LazyQuotes

	if !config.Reader.ValidateColumns {
		csvReader.FieldsPerRecord = -1
	}

	return csvReader
}

func toRune(s string) rune {
	runes := []rune(s)

	if len(runes) == 0 {
		fmt.Printf("Empty delimiter, will default to ;\n")

		return ';'
	}

	if len(runes) > 1 {
		fmt.Printf("Invalid multichar delimiter: %s\n", s)
	}

	return runes[0]
}
